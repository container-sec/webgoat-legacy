FROM openjdk:8-jre-alpine

ARG WEBGOAT_VERSION=6.0.1

RUN apk update && apk add
RUN adduser --system --home /home/webgoat webgoat
RUN cd /home/webgoat/;
RUN chgrp -R 0 /home/webgoat
RUN chmod -R g=u /home/webgoat
RUN apk add ca-certificates libstdc++ glib curl unzip

USER webgoat
WORKDIR /home/webgoat

COPY target/WebGoat-${WEBGOAT_VERSION}-war-exec.jar /home/webgoat/webgoat.jar

EXPOSE 9000

CMD java -jar /home/webgoat/webgoat.jar -httpPort 9000